package com.dirakto.projekt3

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat

class NotificationBroadcast : BroadcastReceiver(){

    override fun onReceive(context: Context, intent: Intent) {
        val notification = NotificationCompat.Builder(context, "com.dirakto.projekt3").apply {
            setContentTitle("New Article")
            setSmallIcon(R.drawable.ic_launcher_foreground)
            setAutoCancel(true)
        }.build()
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(0, notification)
    }
}