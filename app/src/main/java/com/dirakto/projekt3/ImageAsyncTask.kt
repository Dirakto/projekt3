package com.dirakto.projekt3

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import java.net.URL

class ImageAsyncTask() : AsyncTask<String, Void, Bitmap>(){

    override fun doInBackground(vararg params: String?): Bitmap {
        val url = URL(params[0])
        return BitmapFactory.decodeStream(url.openStream())
    }

//    override fun onPostExecute(result: Bitmap) {
////        super.onPostExecute(result)
//    }
}