package com.dirakto.projekt3

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.transition.TransitionManager
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    private lateinit var constraintSet1: ConstraintSet
    private lateinit var constraintSet2: ConstraintSet
    private lateinit var cache: ConstraintLayout
    private var switchProp: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this, ArticleActivity::class.java))

        constraintSet1 = ConstraintSet()
        constraintSet2 = ConstraintSet()
        constraintSet2.clone(this, R.layout.second_activity)
        setContentView(R.layout.activity_main)
        cache = findViewById(R.id.con1)
        constraintSet1.clone(cache)


        mAuth = FirebaseAuth.getInstance()

        button2.setOnClickListener{
            if(switchProp) {
                changeConstraint(it)
            }else{
                val email = editText3.text.toString()
                val pass = editText2.text.toString()
                val pass2 = editText4.text.toString()
                if(pass != pass2)
                    Toast.makeText(this, "Password doesn't match", Toast.LENGTH_SHORT).show()
                else {
                    mAuth.createUserWithEmailAndPassword(email, pass)
                        .addOnCompleteListener {task ->
                            if(task.isSuccessful){
//                                user = mAuth.currentUser
                                changeConstraint(it)
                                Toast.makeText(this, "Successfully added", Toast.LENGTH_LONG).show()
                            }else
                                Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                        }
                }
            }
        }

        button.setOnClickListener {
            if(switchProp) {
                mAuth.signInWithEmailAndPassword(editText3.text.toString(), editText2.text.toString())
                    .addOnCompleteListener {task ->
                        if (task.isSuccessful) {
//                            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, ArticleActivity::class.java))
                        } else
                            Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                    }
            }else
                changeConstraint(it)
        }
    }

    private fun changeConstraint(view: View){
        TransitionManager.beginDelayedTransition(cache)
        if(!switchProp){
            constraintSet1.applyTo(cache)
            findViewById<Button>(R.id.button).text = resources.getString(R.string.signIn)
            clearFields()
        }else{
            constraintSet2.applyTo(cache)
            findViewById<Button>(R.id.button).text = resources.getString(R.string.cancel)
        }
        switchProp = !switchProp
    }

    private fun clearFields(){
        findViewById<EditText>(R.id.editText3).text.clear()
        findViewById<EditText>(R.id.editText2).text.clear()
        findViewById<EditText>(R.id.editText4).text.clear()
    }

}
