package com.dirakto.projekt3

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PatternMatcher
import android.util.Log
import kotlinx.android.synthetic.main.activity_article_view.*

class ArticleViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_view)

        val bundle = intent.extras

        textView5.text = bundle?.getString("title")
        textView6.text = bundle?.getString("desc")!!.trim()

        sendBroadcast(Intent("com.dirakto.projekt3"))
    }
}
