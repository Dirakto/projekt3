package com.dirakto.projekt3

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import kotlinx.android.synthetic.main.activity_article.*
import kotlinx.android.synthetic.main.myfragment_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.io.Serializable

class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm), Serializable{

    private var tvnArticles = listOf<Item>()
    var favArticles = mutableListOf<Item>()

    init{
        setAdapter(this)

        var retrofit: Retrofit = Retrofit.Builder()
//            .baseUrl("https://www.tvn24.pl/")
            .baseUrl("https://www.polsatnews.pl/")
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

        val service = retrofit.create(Repo::class.java)
        val tmp = service.articles().enqueue(object : Callback<Rss> {
            override fun onFailure(call: Call<Rss>, t: Throwable) {
                Log.e("XD", t.message)
            }
            override fun onResponse(call: Call<Rss>, response: Response<Rss>) {
                tvnArticles = response.body()!!.channel.list
                fragments[0].updateList(tvnArticles)
                fragments[1].updateList(favArticles)
            }
        })

    }

    fun addFavArticle(i: Int){
        if(favArticles.contains(tvnArticles[i])) {
            favArticles.remove(tvnArticles[i])
            fragments[1].remove(tvnArticles[i])
        }else {
            favArticles.add(tvnArticles[i])
            fragments[1].addItem(tvnArticles[i])
        }
    }

    companion object {
        @JvmStatic
        private var _myadapter: MyAdapter? = null

        @JvmStatic
        private fun setAdapter(a: MyAdapter){
            if(_myadapter == null)
                _myadapter = a
        }

        @JvmStatic
        fun changeFavourites(i: Int){
            _myadapter?.addFavArticle(i)
        }
    }
    private var fragments = listOf(
        MyFragment().apply{ this.arguments = Bundle().apply { this.putBoolean("canDelete", true) }},
        MyFragment().apply{ this.arguments = Bundle().apply { this.putBoolean("canDelete", false) }}
    )

    override fun getPageTitle(position: Int): CharSequence? = when(position){
        0 -> "New"
        1 -> "Favourites"
        else -> "?"
    }

    override fun getItem(p0: Int): Fragment = fragments[p0]

    override fun getCount(): Int = fragments.size
}