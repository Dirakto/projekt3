package com.dirakto.projekt3

import retrofit2.Call
import retrofit2.http.GET

interface Repo {

//    @GET("najnowsze.xml")
    @GET("rss/biznes.xml")
    fun articles(): Call<Rss>

}