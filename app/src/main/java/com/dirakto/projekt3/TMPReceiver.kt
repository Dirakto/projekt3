package com.dirakto.projekt3

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class TMPReceiver : BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        MyAdapter.changeFavourites(intent?.extras?.getInt("item")!!)
    }
}