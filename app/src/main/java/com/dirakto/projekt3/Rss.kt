package com.dirakto.projekt3

import android.graphics.Bitmap
import org.simpleframework.xml.*
import java.io.Serializable

@Root(strict=false, name="rss")
data class Rss @JvmOverloads constructor(
    @field:Element(name="channel")  @param:Element(name="channel") var channel: Channel
)

@Root(strict=false, name="channel")
data class Channel @JvmOverloads constructor(
    @field:Element(name="title") @param:Element(name="title") var title: String,
    @field:ElementList(inline=true, required=false) @param:ElementList(inline=true, required=false) var list: MutableList<Item>
)

@Root(strict=false, name="item")
class Item  @JvmOverloads constructor(
    @field:Element(data=true, name="title") @param:Element(data=true, name="title") var title: String,
    @field:Element(data=true, name="description") @param:Element(data=true, name="description") var description: String,
    @field:ElementList(inline=true, required=false) @param:ElementList(inline=true, required=false) var enclosure: List<Enclosure>,
    @field:Element(name="guid") @param:Element(name="guid") var guid: String
)
{
    var bmp: Bitmap? = null
}


@Root(strict=false, name="enclosure")
data class Enclosure @JvmOverloads constructor(
    @field:Attribute(name="url") @param:Attribute(name="url") var url: String
)