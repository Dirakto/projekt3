package com.dirakto.projekt3

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item.view.*

class MyRecycler(private val canDelete: Boolean) : RecyclerView.Adapter<MyViewHolder>(){

    var list: MutableList<Item> = mutableListOf()
    var listStatic: List<Item> = listOf()

//    private val regexImg = Regex("(^<img[\\n\\s\\w=\":/\\-\\.?&;]+\\/>)")
//    private  val regexSrc = Regex("\"[\\w\\d:/\\-.?&;=]+\"")

    private var bmp: Bitmap? = null
    private var context: Context? = null


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        bmp = BitmapFactory.decodeResource(p0.resources, R.drawable.baseline_favorite_black_18dp)
        context = p0.context
        val item = LayoutInflater.from(p0.context).inflate(R.layout.item, p0, false)
        return MyViewHolder(item)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.element.textView.text = list[p1].title

        if(list[p1].bmp == null)
            list[p1].bmp = ImageAsyncTask().execute(list[p1].enclosure[0].url).get()

//        val tmp = list[p1].description.trim()
//        val matchResult = regexImg.find(tmp)?.value?.trim()
//        list[p1].description = regexImg.replace(tmp, "").trim()
//
//        if(matchResult != null){
//            var src = regexSrc.find(matchResult)?.value!!.trim()
//            src = src.substring(1, src.length - 1)
//            list[p1].url = ImageAsyncTask().execute("http://www.mtrzaska.com/sites/default/files/MT-thumbnail-01v3%20%281%29.png").get()
//        }
        p0.element.desc.text = list[p1].description
        p0.element.imageView.setImageBitmap(list[p1].bmp)

        p0.element.setOnClickListener{
//            val intent = Intent(it.context, ArticleViewActivity::class.java)
//            val bundle = Bundle()
//            bundle.putString("title", list[p1].title)
//            bundle.putString("desc", list[p1].description)
//            intent.putExtras(bundle)
//            startActivity(it.context, intent, null)

            val uri = list[p1].guid
            val ctiBuilder = CustomTabsIntent.Builder()
            var tmpIntent = Intent()
            tmpIntent.putExtra("item", listStatic.indexOf(list[p1]))
            tmpIntent.setClass(context, TMPReceiver::class.java)
            ctiBuilder.setActionButton(bmp!!, "fav", PendingIntent.getBroadcast(p0.element.context, 1, tmpIntent, PendingIntent.FLAG_UPDATE_CURRENT), false)
            ctiBuilder.build().launchUrl(p0.element.context, Uri.parse(list[p1].guid))

            if(canDelete) {
                list.removeAt(p1)
                this.notifyItemRemoved(p1)
                this.notifyItemRangeChanged(p1, list.size)
            }
        }
    }

}

class MyViewHolder(val element: View): RecyclerView.ViewHolder(element)