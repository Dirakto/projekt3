package com.dirakto.projekt3

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_article.*

class ArticleActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        mAuth = FirebaseAuth.getInstance()

        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = createNotificationChannel(NotificationChannel("com.dirakto.projekt3", "General", NotificationManager.IMPORTANCE_DEFAULT))
            }
        }

        registerReceiver(NotificationBroadcast(), IntentFilter("com.dirakto.projekt3"))

        viewPager.adapter = MyAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

}


