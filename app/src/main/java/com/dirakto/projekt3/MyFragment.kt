package com.dirakto.projekt3

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class MyFragment() : Fragment(){

    private lateinit var recycler: MyRecycler

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.myfragment_layout, container, false).apply {
            findViewById<RecyclerView>(R.id.recycler).apply{
                layoutManager = LinearLayoutManager(this@MyFragment.context)
                recycler = MyRecycler(arguments!!.getBoolean("canDelete"))
                adapter = recycler

            }
        }
    }

    fun updateList(list: List<Item>){
        recycler.list = list.toMutableList()
        recycler.listStatic = list
        recycler.notifyDataSetChanged()
    }

    fun addItem(i: Item){
        recycler.list.add(i)
        recycler.listStatic = recycler.list.toList()
        recycler.notifyItemInserted(recycler.list.size)
    }

    fun remove(i: Item){
        val pos = recycler.list.indexOf(i)
        recycler.list.remove(i)
        recycler.notifyItemRemoved(pos)
    }

}